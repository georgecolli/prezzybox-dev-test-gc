﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Net;
using System.Threading.Tasks;
using Prezzybox.DeveloperTest.Data.Models;




namespace Prezzybox.DeveloperTest.Data.Tests.Requests
{
    public class SingleUserRequestTests
    {
        //Made to instantiate a User object to use in tests, however struggled with making tests that worked.
        private readonly User _user = new User
        {
            Id = 1,
            Email = "hello@123hello.com",
            FirstName = "George",
            LastName = "Collingridge"
        };

        [Fact]
        public void ShouldGetUser()
        {
            // var request = new Data.Requests.SingleUserRequest(2);
            // Assert.Equal(2, response.Id);
            //Assert.Equal("janet.weaver@reqres.in", response.Email);
        }

        //The plan with this was to check if the success code was a sucess or not, and if not assert it to null - as the function should've returned null if it wasn't successful.

        [Fact]
        public void ShouldGetNullWhenNotFound()
        {
            // when a user is not found, the response should be null
            WebException we = new WebException();
            HttpWebResponse errorResponse = we.Response as HttpWebResponse;
            if (errorResponse.StatusCode == HttpStatusCode.NotFound)
            {
                var request = new Data.Requests.SingleUserRequest(2);
                Assert.Equal(null, request);

            }

        }

        [Fact]
        public void ShouldGetPageOfDataWithUserIdOn()
        {
            var response = Data.Requests.ListUsersRequest.GetPageWithUser(3, 12).Result;

            Assert.Contains(response.Data, (u) => u.Id == 12);
            Assert.Equal(3, response.Data.Count);

            response = Data.Requests.ListUsersRequest.GetPageWithUser(2, 6).Result;
            Assert.Contains(response.Data, (u) => u.Id == 6);
            Assert.Equal(2, response.Data.Count);
        }
    }
}
