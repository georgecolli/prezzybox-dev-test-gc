﻿using Prezzybox.DeveloperTest.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
namespace Prezzybox.DeveloperTest.Data.Requests
{
    public class ListUsersRequest : Shared.RequestBase<Models.PagedList<Models.User>>
    {
        static HttpClient client = new HttpClient();
        public string Path = "https://reqres.in/api/users";
        public int Page;
        public int PerPage;
        public ListUsersRequest(int page, int perPage)
        {
            Page = page;
            PerPage = perPage;
        }

        public override async Task<PagedList<User>> GetResponse()
        {
            PagedList<User> user = null;
            HttpResponseMessage response = await client.GetAsync(Path);
            if (response.IsSuccessStatusCode)
            {
                user = await response.Content.ReadAsAsync<PagedList<User>>();
            }
            return user;
        }

        /// <summary>
        /// Gets a page of results which contains the user with the given id
        /// </summary>
        /// <param name="perPage">How many items per page</param>
        /// <param name="id">The id we are searching for</param>
        /// <returns>Page of results containing the users id</returns>
        public static async Task<PagedList<User>> GetPageWithUser(int perPage, int id)
        {
            //NOTE: assume that the API will not return IDs in sequential order
            PagedList<User> listUser = null;
            HttpResponseMessage response = await client.GetAsync($"https://reqres.in/api/{perPage}/{id}");
            if (response.IsSuccessStatusCode)
            {
                listUser = await response.Content.ReadAsAsync<PagedList<User>>();
            }
            return listUser;
        }

        /// <summary>
        /// Takes the results and converts them to an in memory CSV
        /// </summary>
        /// <returns>CSV data</returns>
        public static Task<byte[]> ToCsv()
        {
            throw new NotImplementedException();

        }
    }
}
