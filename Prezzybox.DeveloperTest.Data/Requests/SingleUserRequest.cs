﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Prezzybox.DeveloperTest.Data.Models;

namespace Prezzybox.DeveloperTest.Data.Requests
{
    public class SingleUserRequest : Shared.RequestBase<Models.User>
    {
        static HttpClient client = new HttpClient();
        public long Id;
        public string Path = "https://reqres.in/api/users";

        public SingleUserRequest(int id)
        {
            Id = id;
        }

        public override async Task<Models.User> GetResponse()
        {
            Models.User singleUser = null;
            HttpResponseMessage response = await client.GetAsync($"{Path}/{Id}");
            if (response.IsSuccessStatusCode)
            {
                singleUser = await response.Content.ReadAsAsync<Models.User>();
            }
            return singleUser;
        }
    }
}
