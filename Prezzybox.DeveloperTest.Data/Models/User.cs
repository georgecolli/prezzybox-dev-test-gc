﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prezzybox.DeveloperTest.Data.Models
{
    /// <summary>
    /// A single user
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets unique ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName
        /// </summary>
        public string LastName { get; set; }
    }
}
